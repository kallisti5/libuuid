# libuuid

This is a cleanly BSD licensed libuuid extracted from util-linux 2.34.

## Compiling

```
scons
```

## Installing

> --prefix (and other) options are available to modify the installation directories.

```
scons install
```

## Changelog

* Remove questionable LGPL sources mixed in with libuuid.
* Write a small random provider using /dev/random or /dev/urandom
* Use scons for build, because not autotools.

# License

This library is free software; you can redistribute it and/or
modify it under the terms of the Modified BSD License.
